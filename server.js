const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');
const users = require('./models/users.json');

require('./passport.config');

server.listen(3000);

app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(bodyParser.json());

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});
  app.get('/login', function (req, res) {
    res.sendFile(path.join(__dirname, 'public/login.html'));
  });
  app.get('/race',passport.authenticate('jwt',{session:false}), function (req, res) {
    res.sendFile(path.join(__dirname, 'models/race.json'));
  });
  app.post('/login', function (req, res) {
    const userFromReq = req.body;
    const userInDB = users.find(user => user.login === userFromReq.login);
    if (userInDB && userInDB.password === userFromReq.password) {
      const token = jwt.sign(userFromReq, 'secret');
      res.status(200).json({ auth: true, token });
    } else {
      res.status(401).json({ auth: false });
    }
  });
  let players={};
  let text=Math.floor(Math.random() *4);
  io.on('connection', socket => {
    socket.on('connection', payload => {//проверка подключения
      const {token} = payload;
      let userLogin = jwt.verify(token,'secret').login;
      if(userLogin){   
        players[socket.id]={login:userLogin,score:0,time:0,inTeam:false};
        socket.broadcast.emit('greet', {player:players[socket.id]});
        socket.emit('greet',{player:players[socket.id],isme:true});
      }
    });
    socket.on('disconnect',()=>{
      socket.broadcast.emit("leaved",{player:players[socket.id]});
      delete players[socket.id];
    });
    socket.on("joinrace",payload=>{
      io.of('/').in('race').clients((error, socketIds) => {
        if (error) throw error;
        socketIds.forEach(socketId => io.sockets.sockets[socketId].leave('race'));
      
      });
      const {token} = payload;
      let userLogin = jwt.verify(token,'secret').login;
      if(userLogin){
          players[socket.id].time=payload.time;
          const first=Object.values(players)[0];
          const now=new Date().getTime()/1000;
          let wait=30;
          const match=()=>{
            if(Object.keys(io.in("race").clients()["sockets"]).length>1){
              console.log(Object.keys(io.in("race").clients()["sockets"]).length);
              if((now-first.time)-(now-payload.time)<=wait) {
                socket.join("race");
                players[socket.id].inTeam=true;
                socket.emit("you are in",{login:userLogin});
                io.emit("your team",players);
                socket.emit("your text",text);
                socket.emit("start");
              } else {
                socket.emit("unable to join");
              }
            }else{
              wait+=10;
              console.log(wait);
              setTimeout(match,10000);
            }
          } 
          setTimeout(match,1000)
      }
    });
});