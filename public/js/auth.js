window.onload = () => {
    const jwt = localStorage.getItem('jwt');
    let text="";
    let player=[];
    if (jwt) {
        const socket = io.connect('http://localhost:3000');
        socket.emit('connection', {token: jwt });
        socket.on('greet', payload => {
            console.log("hi "+payload.player.login);
        });
        socket.on('leaved',payload=>{
            console.log(payload.player.login+"leaved");
        });
        const time=new Date().getTime()/1000;
        socket.emit("joinrace",{token:jwt,time:time});
        socket.on("you are in",payload=>{
            console.log(payload.login+"plays");
            socket.on("your team",payload=>{
                Object.values(payload).forEach(element => {
                    if(element.inTeam==true) player.push(element.login);
                });
                player=player.filter((value, index, self) => {
                    return self.indexOf(value) === index
                  });
                  createTeamList(player);
            })
            socket.on("start",()=>{
                console.log("lets do it");
            })
        });
        socket.on("unable to join",()=>{
            console.log("Please,wait for next race!");
        })
        socket.on("your text",payload=>{
            fetch("/race",{
                method:"GET",
                headers:{'Authorization':'Bearer '+jwt}
            }).then(res => {
                res.json().then(body => {
                    if (body) {
                       text=body[payload].text;
                       console.log(text); 
                    } else {
                        console.log('auth failed');
                    }
                })
            }).catch(err => {
                console.log('request went wrong');
            })
        })
    } else {
        location.replace('/login');
    }
}
function createInterface(text){
    document.createElement("div");
    for(let i=0;i<text.length;i++){
        
    }
}
function createTeamList(team){
        document.getElementById("team").innerHTML=team;
};